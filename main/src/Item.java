/**
 * Created by Nati on 09.02.2018.
 */
public class Item {

	private String name;
	private String details;


	Item(String name) {
		this.name = name;
	}

	Item(String name, String details) {
		this.name = name;
		this.details = details;

	}

	@Override
	public String toString() {
		if (details == null || details == "") {
			return name;
		}
		return name + " - " + details;

	}

	public String getName() {
		return name;
	}


	public String getDetails() {
		return details;
	}

	public void setDetails() {
		this.details = details;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Item item = (Item) o;

		if (name != null ? !name.equals(item.name) : item.name != null) return false;
		return details != null ? details.equals(item.details) : item.details == null;

	}

	@Override
	public int hashCode() {
		int result = name != null ? name.hashCode() : 0;
		result = 31 * result + (details != null ? details.hashCode() : 0);
		return result;
	}
}
