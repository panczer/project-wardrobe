/**
 * Created by Nati on 10.02.2018.
 */

import java.util.Arrays;

public class Drawer {

	private Item[] itemsInside;
	private int maxSize;
	private int actualSize = 0;

	public int getMaxSize() {
		return maxSize;
	}

	public int getActualSize() {
		return actualSize;
	}

	public Item[] getItemsInside() {
		return itemsInside;
	}

	Drawer() {
		this.maxSize = 10;
		itemsInside = new Item[this.maxSize];
	}

	Drawer(int maxSize) {

		if (maxSize > 0) {
			this.maxSize = maxSize;
			itemsInside = new Item[this.maxSize];
		} else {
			this.maxSize = 10;
			itemsInside = new Item[this.maxSize];
			System.out.println("Podano zbyt małą liczbę, zainicjalizowano wartoscia domyślną 10");
		}

	}

	void addItem(Item plusItem) {
		if (actualSize < maxSize) {
			itemsInside[actualSize] = plusItem;
			actualSize++;
		} else {
			System.out.println("Brak miejsca w szufladzie");
		}
	}

	void wyswietl() {
		for (int i = 0; i < maxSize; i++) {
			System.out.println(itemsInside[i]);
		}
	}

	Item pickItem(String itemName) {
		for (int i = 0; i < actualSize; i++) {
			String nazwa = itemsInside[i].getName();
			if (nazwa == itemName) {
				Item thisItem = itemsInside[i];
				for (int j = i; j < actualSize - 1; j++) {
					itemsInside[j] = itemsInside[j + 1];
				}
				actualSize--;
				return thisItem;
			}
		}
		System.out.println("Brak rzeczy o nazwie " + itemName);
		return null;
	}

	Item pickFirstItem() {
		Item firstItem = itemsInside[0];
		if (itemsInside[0] == null) {
			System.out.println("Brak rzeczy w szufladzie");
			return null;
		}
		for (int i = 0; i < actualSize - 1; i++) {
			itemsInside[i] = itemsInside[i + 1];
		}
		actualSize--;
		return firstItem;
	}

	boolean checkIfItemExists(String findName) {
		for (int i = 0; i < actualSize; i++) {
			String nazwa = itemsInside[i].getName();
			if (nazwa.equals(findName)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public String toString() {
		String showNameAndDetails = "";
		for (int i = 0; i < actualSize; i++) {
			showNameAndDetails += itemsInside[i].toString();
			if (i != actualSize - 1) {
				showNameAndDetails += "\n";
			}
		}
		return showNameAndDetails;
	}

	void sortByName() {

		int score;
		Item zmiena;
		for (int i = 0; i < actualSize; i++) {
			for (int j = 0; j < i; j++) {
				score = itemsInside[i].getName().compareTo(itemsInside[j].getName());
				if (score < 0) {
					zmiena = itemsInside[j];
					itemsInside[j] = itemsInside[i];
					itemsInside[i] = zmiena;
				} else {
					continue;
				}
				}
			}

// 		Item[] tab = new Item[actualSize];
//
//		for (int i = 0; i < actualSize; i++) {
//			tab[i] = itemsInside[i];
//		}
//		Arrays.sort(tab);
//
//		for (int i = 0; i < actualSize; i++) {
//			System.out.println(tab[i]);
//		}

		}

	void sortByDetails() {
		int score;
		Item zmienna;
		Item[] tab = new Item[actualSize];
		int rozmiarTab = 0;

		for (int i = 0; i < actualSize; i++) {
			if (i == 0) {
				if (itemsInside[i].getDetails() != null) {
					break;
				} else {
					tab[rozmiarTab] = itemsInside[i];
					rozmiarTab++;
					for (int a = i; a < actualSize - 1; a++) {
						itemsInside[a] = itemsInside[a + 1];
					}
					actualSize--;
					i--;
				}
			}
			for (int j = 0; j < i; j++) {

				if (itemsInside[i].getDetails() != null) {
					score = itemsInside[i].getDetails().compareTo(itemsInside[j].getDetails());
					if (score < 0) {
						zmienna = itemsInside[j];
						itemsInside[j] = itemsInside[i];
						itemsInside[i] = zmienna;
					}
					if (score > 0) {
						continue;
					}
				} else {
					tab[rozmiarTab] = itemsInside[i];
					rozmiarTab++;
					for (int a = i; a < actualSize - 1; a++) {
						itemsInside[a] = itemsInside[a + 1];
					}
					actualSize--;
					i--;
				}


			}
		}
		for (int i = 0; i < rozmiarTab; i++) {
			itemsInside[actualSize] = tab[i];
			actualSize++;
		}

	}
//		for (int i=itemsInside.length; i<maxSize; i++) {
//			for (int j = 0; j< tab.length; j++) {
//				itemsInside[i] = tab[0];
//				continue;
//			}
//		}


	boolean checkIfItemCanBeAdded() {
		return actualSize < maxSize;
	}

	boolean checkIfDrawerIsEmpty() {
		boolean checkIfDrawerIsEmpty = true;
		if (actualSize != 0) {
			checkIfDrawerIsEmpty = false;
		}
		return checkIfDrawerIsEmpty;
	}
}
