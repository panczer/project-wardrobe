/**
 * Created by Nati on 09.02.2018.
 */
public class Wardrobe {

	Drawer[] drawers;
	int actualDrawersCount;

	Wardrobe() {
		actualDrawersCount = 4;
		drawers = new Drawer[actualDrawersCount];
		for (int i = 0; i < actualDrawersCount; i++) {
			drawers[i] = new Drawer();
		}
	}

	Wardrobe(int drawersCount, int maxDrawerSize) {
		actualDrawersCount = drawersCount;
		drawers = new Drawer[drawersCount];
		for (int i = 0; i < drawersCount; i++) {
			drawers[i] = new Drawer(maxDrawerSize);
		}
	}



}

