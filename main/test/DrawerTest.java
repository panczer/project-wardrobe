import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;


/**
 * Created by pancz on 10.02.2018.
 */
public class DrawerTest {

	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();

	private static final String NAME = "nazwa";
	private static final String NAME_ONLY = "nazwaTylko";

	private static final String DETAIL = "szczegol";

	private static final int DEFAULT_SIZE = 10;

	Drawer testDrawerBothValues;
	Drawer testDrawerNameOnly;

	List<Item> itemListBothValues = new ArrayList<>();
	List<Item> itemListNameOnly = new ArrayList<>();

	@Before
	public void beforeTest() {
		System.setOut(new PrintStream(outContent));
		testDrawerBothValues = null;
		testDrawerNameOnly = null;
		itemListBothValues = new ArrayList<>();
		itemListNameOnly = new ArrayList<>();
		outContent.reset();
	}

	private void prepareTestDrawer() {
		testDrawerBothValues = new Drawer();
		testDrawerNameOnly = new Drawer();
		for (int i = 0; i < DEFAULT_SIZE; i++) {
			itemListBothValues.add(new Item(NAME + i, DETAIL + i));
			itemListNameOnly.add(new Item(NAME_ONLY + i));
			testDrawerBothValues.addItem(itemListBothValues.get(i));
			testDrawerNameOnly.addItem(itemListNameOnly.get(i));
		}
	}

	@After
	public void afterTest() {
		System.setOut(System.out);
	}

	/***********************************************************************************/
	/** CONSTRUCTORS */
	/***********************************************************************************/

	@Test
	public void noParameterConstructorTest() {
		Drawer drawer = new Drawer();
		assertEquals("maxSize powinno byc 10", DEFAULT_SIZE, drawer.getMaxSize());
		assertEquals("Wielkosc tablicy powinna byc 10", DEFAULT_SIZE, drawer.getItemsInside().length);
		assertEquals("", outContent.toString());

	}

	@Test
	public void negativeParameterConstructorTest() {
		Drawer drawer = new Drawer(0);
		assertEquals("maxSize powinno byc 10", DEFAULT_SIZE, drawer.getMaxSize());
		assertEquals("Wielkosc tablicy powinna byc 10", DEFAULT_SIZE, drawer.getItemsInside().length);
		assertEquals("Podano zbyt małą liczbę, zainicjalizowano wartoscia domyślną 10\r\n", outContent.toString());

	}

	/***********************************************************************************/
	/** ADD ITEM */
	/***********************************************************************************/

	@Test
	public void addItemTest() {
		Drawer drawer = new Drawer();
		drawer.addItem(new Item(NAME_ONLY));

		assertEquals("actualSize powinno byc 1", 1, drawer.getActualSize());
		assertEquals("tablica powinna miec wielkosc 10", 10, drawer.getItemsInside().length);
		assertNotNull(drawer.getItemsInside()[0]);
		assertEquals(NAME_ONLY, drawer.getItemsInside()[0].getName());
		assertNull(drawer.getItemsInside()[0].getDetails());

		drawer.addItem(new Item(NAME, DETAIL));

		assertEquals("actualSize powinno byc 2", 2, drawer.getActualSize());
		assertEquals("tablica powinna miec wielkosc 10", 10, drawer.getItemsInside().length);
		assertNotNull(drawer.getItemsInside()[0]);
		assertNotNull(drawer.getItemsInside()[1]);
		assertEquals(NAME_ONLY, drawer.getItemsInside()[0].getName());
		assertNull(drawer.getItemsInside()[0].getDetails());
		assertEquals(NAME, drawer.getItemsInside()[1].getName());
		assertEquals(DETAIL, drawer.getItemsInside()[1].getDetails());
	}

	@Test
	public void addItemToFullDrawerTest() {
		prepareTestDrawer();
		outContent.reset();
		testDrawerBothValues.addItem(new Item(NAME, DETAIL));

		assertEquals(DEFAULT_SIZE, testDrawerBothValues.getActualSize());
		assertEquals(DEFAULT_SIZE, testDrawerBothValues.getMaxSize());
		for (int i = 0; i < DEFAULT_SIZE; i++) {
			assertEquals(NAME + i, testDrawerBothValues.getItemsInside()[i].getName());
			assertEquals(DETAIL + i, testDrawerBothValues.getItemsInside()[i].getDetails());
		}
		assertEquals("Brak miejsca w szufladzie\r\n", outContent.toString());
	}

	/***********************************************************************************/
	/** PICK ITEM */
	/***********************************************************************************/

	@Test
	public void pickItemTest() {
		Drawer drawer = new Drawer();
		Item addingItem = new Item(NAME_ONLY);
		drawer.addItem(addingItem);
		Item pickedItem = drawer.pickItem(NAME_ONLY);

		assertEquals(addingItem.getName(), pickedItem.getName());
		assertEquals(addingItem.getDetails(), pickedItem.getDetails());
		assertEquals(0, drawer.getActualSize());
		assertEquals(DEFAULT_SIZE, drawer.getMaxSize());
	}

	@Test
	public void pickItemWithSameNamesTest() {
		Drawer drawer = new Drawer();
		Item addingItem = new Item(NAME_ONLY);
		Item addingBothValuesItem = new Item(NAME, DETAIL);
		drawer.addItem(addingItem);
		drawer.addItem(addingBothValuesItem);
		drawer.addItem(addingItem);
		Item pickedItem = drawer.pickItem(NAME_ONLY);

		assertEquals(addingItem.getName(), pickedItem.getName());
		assertEquals(addingItem.getDetails(), pickedItem.getDetails());

		assertEquals(2, drawer.getActualSize());
		assertEquals(DEFAULT_SIZE, drawer.getMaxSize());

		assertNotNull(drawer.getItemsInside()[0]);
		assertEquals(NAME, drawer.getItemsInside()[0].getName());
		assertEquals(DETAIL, drawer.getItemsInside()[0].getDetails());

		assertNotNull(drawer.getItemsInside()[1]);
		assertEquals(NAME_ONLY, drawer.getItemsInside()[1].getName());
		assertEquals(null, drawer.getItemsInside()[1].getDetails());
	}

	@Test
	public void pickItemFromEmptyDrawerTest() {
		Drawer drawer = new Drawer();

		Item pickedItem = drawer.pickItem(NAME_ONLY);
		assertNull(pickedItem);
		assertEquals(0, drawer.getActualSize());
		assertEquals(DEFAULT_SIZE, drawer.getMaxSize());
		assertEquals("Brak rzeczy o nazwie " + NAME_ONLY + "\r\n", outContent.toString());
	}

	@Test
	public void pickItemForNotExistingNameDrawerTest() {
		Drawer drawer = new Drawer();
		Item addingItem = new Item(NAME_ONLY);
		drawer.addItem(addingItem);
		Item pickedItem = drawer.pickItem(NAME);

		assertNull(pickedItem);
		assertEquals(1, drawer.getActualSize());
		assertEquals(DEFAULT_SIZE, drawer.getMaxSize());
		assertNotNull(drawer.getItemsInside()[0]);
		assertEquals(NAME_ONLY, drawer.getItemsInside()[0].getName());

		assertEquals("Brak rzeczy o nazwie " + NAME + "\r\n", outContent.toString());
	}

	@Test
	public void pickFirstItemTest() {
		prepareTestDrawer();

		for (int i = 0; i < DEFAULT_SIZE; i++) {
			Item pickedItem = testDrawerBothValues.pickFirstItem();
			assertNotNull(pickedItem);
			assertEquals(NAME + i, pickedItem.getName());
			assertEquals(DETAIL + i, pickedItem.getDetails());
			assertEquals(DEFAULT_SIZE - i - 1, testDrawerBothValues.getActualSize());
			assertEquals(DEFAULT_SIZE, testDrawerBothValues.getMaxSize());
		}
	}

	@Test
	public void pickFirstItemForEmptyDrawerTest() {
		Drawer drawer = new Drawer();
		Item pickedItem = drawer.pickFirstItem();

		assertNull(pickedItem);
		assertEquals("Brak rzeczy w szufladzie\r\n", outContent.toString());

		assertEquals(0, drawer.getActualSize());
		assertEquals(DEFAULT_SIZE, drawer.getMaxSize());
	}

	/***********************************************************************************/
	/** checkIfItemExists */
	/***********************************************************************************/

	@Test
	public void checkIfItemExistsTest() {
		prepareTestDrawer();
		for (int i = 0; i < DEFAULT_SIZE; i++) {
			assertEquals("Obiekt o nazwie '" + NAME_ONLY + i + "' nie zostal odnaleziony, a powinien być", true,
					testDrawerNameOnly.checkIfItemExists(NAME_ONLY + i));
		}

		assertEquals("Obiektu o nazwie '" + NAME + "' nie ma w tablicy to dlaczego zwrocone jest true?", false,
				testDrawerNameOnly.checkIfItemExists(NAME));
	}

	/***********************************************************************************/
	/** toString */
	/***********************************************************************************/

	@Test
	public void toStringBothValuesTest() {
		prepareTestDrawer();
		assertEquals(prepareExpectedStringForDrawer(testDrawerBothValues), testDrawerBothValues.toString());
	}

	@Test
	public void toStringNameOnlyTest() {
		prepareTestDrawer();
		assertEquals(prepareExpectedStringForDrawer(testDrawerNameOnly), testDrawerNameOnly.toString());
	}

	@Test
	public void toStringEmptyDrawerTest() {
		Drawer drawer = new Drawer();
		assertEquals("", drawer.toString());
	}

	@Test
	public void toStringOneNameOneBothValuesTest() {
		Drawer drawer = new Drawer();
		drawer.addItem(new Item(NAME, DETAIL));
		drawer.addItem(new Item(NAME_ONLY));
		assertEquals(prepareExpectedStringForDrawer(drawer), drawer.toString());
	}

	private String prepareExpectedStringForDrawer(Drawer drawer) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < drawer.getActualSize(); i++) {
			boolean detailsExist = drawer.getItemsInside()[i].getDetails() != null && !drawer.getItemsInside()[i]
					.getDetails().equals("");
			sb.append(drawer.getItemsInside()[i].getName() + (detailsExist ? " - " + drawer.getItemsInside()[i].getDetails() : ""));
			if (i < drawer.getActualSize() - 1) {
				sb.append("\n");
			}
		}
		return sb.toString();
	}

	/***********************************************************************************/
	/** sortByName */
	/***********************************************************************************/

	@Test
	public void sortByNameTest() {
		Drawer drawer = new Drawer();
		drawer.addItem(new Item(NAME + 3, DETAIL + 0));
		drawer.addItem(new Item(NAME + 2, DETAIL + 1));
		drawer.addItem(new Item(NAME + 1, DETAIL + 2));
		drawer.addItem(new Item(NAME + 0, DETAIL + 3));

		drawer.sortByName();
		int testSize = 4;
		for (int i = 0; i < testSize; i++) {
			assertEquals(NAME + i, drawer.getItemsInside()[i].getName());
			assertEquals(DETAIL + (testSize - 1 - i), drawer.getItemsInside()[i].getDetails());
		}
	}

	/***********************************************************************************/
	/** sortByDetails */
	/***********************************************************************************/

	@Test
	public void sortByDetailsTest() {
		Drawer drawer = new Drawer();
		drawer.addItem(new Item(NAME + 3, DETAIL + 0));
		drawer.addItem(new Item(NAME + 2, DETAIL + 1));
		drawer.addItem(new Item(NAME + 1, DETAIL + 2));
		drawer.addItem(new Item(NAME + 0, DETAIL + 3));

		drawer.sortByDetails();
		int testSize = 4;
		for (int i = 0; i < testSize; i++) {
			assertEquals(DETAIL + i, drawer.getItemsInside()[i].getDetails());
			assertEquals(NAME + (testSize - 1 - i), drawer.getItemsInside()[i].getName());
		}
	}

	@Test
	public void sortByDetailsWhenDetailNullTest() {
		Drawer drawer = new Drawer();
		drawer.addItem(new Item(NAME_ONLY + 3));
		drawer.addItem(new Item(NAME_ONLY + 2));
		drawer.addItem(new Item(NAME_ONLY + 1));
		drawer.addItem(new Item(NAME_ONLY + 0));

		drawer.sortByDetails();
		int testSize = 4;
		for (int i = 0; i < testSize; i++) {
			assertEquals(NAME_ONLY + (testSize - 1 - i), drawer.getItemsInside()[i].getName());
		}
	}

	/***********************************************************************************/
	/** checkIfItemCanBeAdded */
	/***********************************************************************************/

	@Test
	public void checkIfItemCanBeAddedTest() {
		Drawer drawer = new Drawer(2);

		assertEquals(true, drawer.checkIfItemCanBeAdded());

		drawer.addItem(new Item(NAME_ONLY));

		assertEquals(true, drawer.checkIfItemCanBeAdded());

		drawer.addItem(new Item(NAME_ONLY+1));

		assertEquals(false, drawer.checkIfItemCanBeAdded());
	}

	/***********************************************************************************/
	/** checkIfItemCanBeAdded */
	/***********************************************************************************/

	@Test
	public void checkIfDrawerIsEmptyTest() {
		Drawer drawer = new Drawer(1);

		assertEquals(true, drawer.checkIfDrawerIsEmpty());

		drawer.addItem(new Item(NAME_ONLY));

		assertEquals(false, drawer.checkIfDrawerIsEmpty());
	}

}
