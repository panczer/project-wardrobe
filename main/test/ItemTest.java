import org.junit.Assert;
import org.junit.Test;

/**
 * Created by pancz on 10.02.2018.
 */
public class ItemTest {

	@Test
	public void toStringBothValuesTest() {
		String name = "Skarpeta";
		String details = "niebieska";
		Item item = new Item(name, details);
		Assert.assertEquals(name + " - " + details, item.toString());
	}

	@Test
	public void toStringDetailsNullTest() {
		String name = "Skarpeta";
		String details = null;
		Item item = new Item(name, details);
		Assert.assertEquals("Gdy w konstruktorze dwuparametrowym przekazemy null do details to takze ma wyswietlac tylko nazwe", name, item.toString());
	}

	@Test
	public void toStringDetailsEmptyStringTest() {
		String name = "Skarpeta";
		String details = "";
		Item item = new Item(name, details);
		Assert.assertEquals("Gdy w konstruktorze dwuparametrowym przekazemy pusty string w details to takze ma wyswietlac tylko nazwe", name, item.toString());
	}

	@Test
	public void toStringOnlyNameTest() {
		String name = "Skarpeta";
		Item item = new Item(name);
		Assert.assertEquals(name, item.toString());
	}
}
